#!/bin/bash
#This is a shell script for using friso to segment txt files which in a same dir.
frisoini_path=/home/tony/文档/friso-0.1/core/friso.ini
workdir=/home/tony/文档/friso-0.1/core
inputdir=
outputdir=
filename=

read -p "Please input the inputdir:" inputdir
read -p "Please input the outputdir:" outputdir
cd $inputdir
echo "************************`date`********************************" >> $workdir/log.txt
for file in *.txt
do
	input="$inputdir/$file"
	output="$outputdir/$file"
	friso -init $frisoini_path -input $input -output $output | tee -a $workdir/log.txt
done
echo "************************`date`********************************" >> $workdir/log.txt
exit 0
