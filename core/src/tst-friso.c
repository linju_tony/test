/*
 * friso test program.
 *
 * @author	chenxin
 * @email	chenxin619315@gmail.com
 */
#include "friso_API.h"
#include "friso.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


/**
 * File Explain.
 * 
 * @author	chenxin
 * @see		http://www.webssky.com 
 */

#define __LENGTH__ 15
#define __INPUT_LENGTH__ 1024
#define ___EXIT_INFO___																\
 		println("Thanks for trying friso.");										\
 		break;

#define ___ABOUT___																	\
 		println("+-----------------------------------------------------------+");	\
 		println("| friso - a chinese word segmentation writen by c.          |");	\
 		println("| bug report email - chenxin619315@gmail.com.               |");	\
 		println("| or: visit http://code.google.com/p/friso.                 |");	\
 		println("|     java edition for http://code.google.com/p/jcseg       |");	\
 		println("| type quit to exit.                                        |");	\
 		println("+-----------------------------------------------------------+");

//read a line from a command line.
static string getLine(FILE *fp, string __dst) {
	register int c;
	register string cs;

	cs = __dst;
	while ((c = getc(fp)) != EOF) {
		if (c == '\n') break;
		*cs++ = c;
	}
	*cs = '\0';

	return (c == EOF && cs == __dst) ? NULL : __dst;
}

/*static void printcode( string str ) {
	int i,length;
	length = strlen( str );
	printf("str:length=%d\n", length );
	for ( i = 0; i < length; i++ ) {
		printf("%d ", str[i] );
	}
	putchar('\n');
}*/

int main(int argc, char **argv) {

	clock_t s_time, e_time;
	char line[__INPUT_LENGTH__];
	int i;

	string __path__ = NULL;
	string __inputpath__ = NULL;
	string __outputpath__ = NULL;
	//char __inputpath__[100];
	//char __outputpath__[100];
	//char * __inputpath__ = NULL;
	//char * __outputpath__ = NULL;

	FILE *fin,*fout,*ftemp;

	friso_t friso;
	friso_task_t task;

	//get the lexicon directory
	for (i = 0; i < argc; i++) {
		if (strcasecmp("-init", argv[i]) == 0) {
			__path__ = argv[i + 1];
		}
		else if (strcasecmp("-input", argv[i]) == 0) {
			__inputpath__ = argv[i+1];
		}
		else if (strcasecmp("-output", argv[i]) == 0) {
			__outputpath__ = argv[i+1];
		}
	}
	printf("__path__=%s\n__inputpath__=%s\n__outputpath__=%s\n", __path__, __inputpath__, __outputpath__);
	//__inputpath__ = argv[1];
	//__outputpath__ = argv[2];
	/*if (__inputpath__ == NULL || __outputpath__ == NULL) {
		println("Usage: friso [inputfilepath] [outputfilepath].");
		exit(1);
	}*/
	//printf("%s\n",__path__);

	ftemp = NULL;
	ftemp = fopen(__path__, "rb");
	if (ftemp == NULL) {
		println("Usage: friso -init lexicon path");
		exit(0);
	} else fclose(ftemp);
	 
	s_time = clock();

	//initialize
/*	friso_t friso = friso_new();
	friso_dic_t dic = friso_dic_new();

	friso_dic_load_from_conf( dic, __path__, __LENGTH__ );
	friso_set_dic( friso, dic );
	friso_set_mode( friso, __FRISO_COMPLEX_MODE__ );*/
	friso = friso_new_from_ifile(__path__);

	e_time = clock();

	printf("friso initialized in %fsec\n", (double)(e_time - s_time) / CLOCKS_PER_SEC);
	___ABOUT___


	//while (scanf("%s", __inputpath__) != 0) {
		//sprintf(__inputpath__, "/home/tony/文档/friso-0.1/core/input/%d.txt", n);
		//sprintf(__outputpath__, "/home/tony/文档/friso-0.1/core/output/%d.txt", n);
		//fin=fopen(__inputpath__,"r+");
		//fout=fopen(__outputpath__,"w+");
		//__inputpath__ = "1.txt";
		//__outputpath__ = "2.txt";
		//printf("%s\n", __inputpath__);
		//printf("%s\n", __outputpath__);
		/*if (strcasecmp(__inputpath__, "quit") == 0) {
			println("Thanks for trying friso.");
			return 0;
		}
		if (scanf("%s", __outputpath__) == 0) {
			continue;
		}*/
		
		/*if (strcasecmp(__filename__, "quit") == 0) {
			println("Thanks for trying friso.");
			fclose(ftemp);
			if (system("rm -f /home/tony/文档/friso-0.1/core/temp.txt") == -1) {
				println("Error deleting for temp.txt");
				return 0;
			}
			return 0;
		}*/
		fin = NULL;
		fout = NULL;

		s_time = clock();
		//open the file
		fin = fopen(__inputpath__, "r+");

		if (fin == NULL) {
			print("Error opening for input file ");
			println(__inputpath__);
			//continue;
			return 1;
		}

		fout = fopen(__outputpath__, "w+");
		
		if (fout == NULL) {
			print("Error openning for output file ");
			println(__outputpath__);
			//continue;
			return 1;
		}

		//set the task.
		task = friso_new_task();

		//s_time = clock();

		while (getLine(fin, line) != NULL) {

			//print("friso>> ");
			//getLine( fin, line );
			//exit the programe
			/*if ( strcasecmp( line, "quit" ) == 0 ) {
				___EXIT_INFO___
			}*/

			//set the task text.
			friso_set_text(task, line);

			//println("分词结果:");

			while ((friso_next(friso, friso->mode, task)) != NULL) {
				//printf("%s[%d,%d]/ ", task->hits->word, task->hits->type, task->hits->offset );
				fprintf(fout, "%s/ ", task->hits->word);
				if (task->hits->type == __FRISO_NEW_WORDS__) {
					FRISO_FREE(task->hits->word);
				}
			}
			fprintf(fout, "\n");
			/* 
			e_time = clock();
			printf("\nDone, cost < %fsec\n", ( (double)(e_time - s_time) ) / CLOCKS_PER_SEC );
			*/
		}

		printf("success output in %s\n", __outputpath__);
		/*if (fin != NULL)
			fclose(fin);
		if (fout != NULL)
			fclose(fout);
			*/

		friso_free_task(task);

		fclose(fin);
		fclose(fout);
		e_time = clock();
		printf("\nDone, cost < %fsec\n", ((double)(e_time - s_time)) / CLOCKS_PER_SEC);
	//}

	
	return 0;
}
